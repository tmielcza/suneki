﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Calculate a map area according to renderer bounds of any group of gamobject.
/// </summary>
public class BoxBounds
{
    private Vector3 maxRange;
    private Vector3 minRange;

    /// <summary>
    /// The range from the center of given area to the max x y z positions.
    /// </summary>
    public Vector3 MaxRange { get { return maxRange; } }
    /// <summary>
    /// The range from the center of gieven area to the min x y z positions.
    /// </summary>
    public Vector3 MinRange { get { return minRange; } }

    /// <summary>
    /// The bounds of the group of gameobjects given for instanciation.
    /// </summary>
    public Bounds BoxBound { get; private set; }
    
    /// <summary>
    /// Initializes a new instance of the Boxbounds class.
    /// Calculates the bounds of the game area using 
    /// </summary>
    /// <param name="Area">Gameobject or group of gameobject representing the game area.</param>
    public BoxBounds(GameObject Area)
    {
        BoxBound = GetBounds(Area);
        maxRange.x = BoxBound.center.x + BoxBound.extents.x - 10f;
        maxRange.y = BoxBound.center.y + BoxBound.extents.y - 10f;
        maxRange.z = BoxBound.center.z + BoxBound.extents.z - 10f;
        minRange.x = BoxBound.center.x - BoxBound.extents.x + 10f;
        minRange.y = BoxBound.center.y - BoxBound.extents.y + 10f;
        minRange.z = BoxBound.center.z - BoxBound.extents.z + 10f;
    }

    /// <summary>
    /// Calculate total bounds of group of gameobject given.
    /// </summary>
    /// <param name="Obj">The group of gameobject representing your map.</param>
    /// <returns>Total bounds.</returns>
    public static Bounds GetBounds(GameObject Obj)
    {
        Bounds bounds;
        Renderer childRender;

        bounds = GetRenderBounds(Obj);
        if (bounds.extents.x == 0f)
        {
            bounds = new Bounds(Obj.transform.position, Vector3.zero);
            foreach (Transform child in Obj.transform)
            {
                childRender = child.GetComponent<Renderer>();
                if (childRender)
                    bounds.Encapsulate(childRender.bounds);
                else
                    bounds.Encapsulate(GetBounds(child.gameObject));
            }
        }
        return bounds;
    }

    // Get bounds of one gameobject using Renderer.
    private static Bounds GetRenderBounds(GameObject objeto)
    {
        Bounds bounds = new Bounds(Vector3.zero, Vector3.zero);
        Renderer render = objeto.GetComponent<Renderer>();
        if (render != null)
            return render.bounds;
        return bounds;
    }
}
