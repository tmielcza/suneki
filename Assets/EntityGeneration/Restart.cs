﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Restart the game. Need to be used with GuiLayer Onclick envent.
/// Add it to the Box gameobject in Unity.
/// </summary>
public class Restart : MonoBehaviour
{
    private FoodGenerator food;
    private PlayerController player;
    private SnakeGenerator snakeGenerator;

    // Get entities.
    void Start()
    {
        snakeGenerator = GameObject.FindGameObjectWithTag(Tags.gameController)
            .GetComponent<SnakeGenerator>();
        food = GameObject.FindGameObjectWithTag(Tags.gameController)
            .GetComponent<FoodGenerator>();
        player = GameObject.Find("PlayerController").GetComponent<PlayerController>();
    }

    // Add delegate for OnRestart envent. See GuiLayer script. 
    void OnEnable()
    {
        GuiLayer.OnRestart += RestartEntities;
    }

    // delete delegates for OnRestart envent. See GuiLayer script.
    void OnDisable()
    {
        GuiLayer.OnRestart -= RestartEntities;
    }

    // reset food and player.
    void RestartEntities()
    {
        System.GC.Collect();
        food.Reset();
        player.Restart();
        snakeGenerator.snake.Reset();
   }
}
