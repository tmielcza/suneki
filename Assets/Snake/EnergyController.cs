﻿using UnityEngine;
using System.Collections;

public class EnergyController : MonoBehaviour
{
    /// <summary>
    /// Amount of volatile power, given by red blocks.
    /// </summary>
    public float volatilePower = 0f;

    private float volatilePowerVariationSpeed;
    private float volatilePowerEnd = 0f;
    private float volatilePowerDropSpeed = -6f;
    private float volatilePowerRaiseSpeed = 50f;
    private float multi = 1f;
    private const float multiCoef = 1.5f;
    private float multiDropSpeed = 3f;
    private float batteryPower = 10f;

    public float power;
    public float canonPower;

    // Energy drop speed.
    public float energyDrop
    {
        get
        {
            return (snakeCapacity / 500f);
        }
    }

    // Single chunk power capacity.
    private const float unitCapacity = 4f;

    /// <summary>
    /// Power capacity of snake.
    /// </summary>
    public float snakeCapacity
    {
        get
        {
            return (tailScript.size * unitCapacity);
        }
    }

    // Volatile power minima for usage in different purposes.
    private float upgradeThreshold = 4f;
    private float fillThreshhold = 30f;

    private Tail tailScript;
    private move moveScript;

    void Awake()
    {
        volatilePowerVariationSpeed = volatilePowerDropSpeed;
        moveScript = this.GetComponent<move>();
        tailScript = this.GetComponent<Tail>();
        volatilePowerEnd = 0f;
    }

    /// <summary>
    /// Raise volatile power. Close calls raise exponentially.
    /// </summary>
    public void PowerUp()
    {
        volatilePowerEnd = volatilePower + batteryPower * multi;
        multi *= multiCoef;
        volatilePowerVariationSpeed = volatilePowerRaiseSpeed;
    }

    private void Update()
    {
        VolatileVary();
        MultiVary();
    }

    // Decrease multi over time.
    private void MultiVary()
    {
        if (multi > 1f)
        {
            multi -= 0.5f * Time.deltaTime;
        }
    }

    // Increase and decrease volatilePower over time.
    private void VolatileVary()
    {
        if (volatilePowerVariationSpeed > 0 && volatilePower >= volatilePowerEnd)
        {
            volatilePowerVariationSpeed = volatilePowerDropSpeed;
            volatilePower = volatilePowerEnd;
            volatilePowerEnd = 0f;
        }
        else if (volatilePowerVariationSpeed < 0 && volatilePower < volatilePowerEnd)
        {
        }
        else
        {
            volatilePower += Time.deltaTime * volatilePowerVariationSpeed * moveScript.speedCoef;
        }
    }
}
