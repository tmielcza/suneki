﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Manage moves for a snake. Add it to a snake.
/// </summary>
public class move : MonoBehaviour
{
    // Contains player data (isAlive).
    private PlayerController player;

    // Current move speed.
    public float moveSpeed = 35f;

    /// <summary>
    /// Regular speed of a snake.
    /// </summary>
    public const float baseSpeed = 35f;

    /// <summary>
    /// Ratio between snake's actual speed and base speed.
    /// </summary>
    public float speedCoef
    {
        get
        {
            return (moveSpeed / baseSpeed);
        }
    }

    /// <summary>
    /// Max rotation value.
    /// </summary>
    public float maxRotSpeed = 1.125f;

    /// <summary>
    /// Min rotation value
    /// </summary>
    public float minRotSpeed = 0.1f;

    /// <summary>
    /// Acceleration on rotation.
    /// </summary>
    public float rotAcceleration = 0.018f;

    /// <summary>
    /// Deceleration in rotation.
    /// </summary>
    public float rotDeceleration = 0.03f;

    // Curent rotation speed.
    private float rotSpeed;

    /// <summary>
    /// How quick will straf run.
    /// </summary>
    public float StrafValue = 0.2f;

    private EnergyController energyController;

    void Awake()
    {
        energyController = this.GetComponent<EnergyController>();
        player = GameObject.Find("PlayerController").GetComponent<PlayerController>();
    }

    // Get needed scripts and init moves values.
    void Start()
    {
        rotSpeed = minRotSpeed;
    }

    // Manage movements.
    void Update()
    {
        if (player.isAlive)
        {
            MoveForward();
            Shift();
        }
        Turn();
    }

    // Manage forward movement with mooth on acceleration.
    void MoveForward()
    {
        if (moveSpeed < baseSpeed)
        {
            moveSpeed += 0.1f;
        }
        moveSpeed = baseSpeed + energyController.volatilePower; // A virer
        transform.Translate(Vector3.forward * moveSpeed * Time.deltaTime);
    }

    // Manage user's rotations orders with smooth accelerations
    private void Turn()
    {
        bool onTurn = false;

        float h = Input.GetAxis("HorizontalLeft");
        float z = Input.GetAxis("VerticalLeft");
        if (h != 0 || z != 0)
            onTurn = true;
        if (onTurn && rotSpeed < maxRotSpeed)
            rotSpeed += rotAcceleration;
        else if (!onTurn && rotSpeed > minRotSpeed)
            rotSpeed -= rotDeceleration;
        h *= rotSpeed;
        z *= rotSpeed;
        transform.Rotate(z, h, 0f);
    }

    // Shift move management.
    private void Shift()
    {
        float h = Input.GetAxis("HorizontalRight") * StrafValue;
        float z = Input.GetAxis("VerticalRight") * StrafValue;
        transform.Translate(h, z, 0f);
    }
}
