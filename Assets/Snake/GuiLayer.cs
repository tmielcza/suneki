﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Add to a camera to display GUI.
/// Have to be added to a Snake prefab.
/// </summary>
public class GuiLayer : MonoBehaviour
{
    // Contains game entities. See EntityGenerator.
    private PlayerController player;
    private Score score;

    private GUIStyle datStyle;
    public Font datFont;
    private EnergyController energy;

    /// <summary>
    /// Delegate for OnRestart event.
    /// </summary>
    public delegate void RestartAction();

    /// <summary>
    /// Add Methods to restart game properly.
    /// </summary>
    public static event RestartAction OnRestart;

    // all buttons.
    private GuiButton[] buttons;

    private int selectedButton;
    private int activatedButton;
    private float lastSelectionTime;
    private Vector2 mousePosition;

    void Awake()
    {
        energy = null;
        buttons = new GuiButton[2];
        buttons[0] = new GuiButton("Restart", new Rect(Screen.width / 2 - 50, 5, 100, 30), RestartButtonAction);
        buttons[1] = new GuiButton("Quit", new Rect(Screen.width / 2 - 50, 40, 100, 30), QuitButtonAction);
    }

    void Start()
    {
        GameObject playerController = GameObject.Find("PlayerController");

        player = playerController.GetComponent<PlayerController>();
        score = playerController.GetComponent<Score>();

        energy = GameObject.Find("Snake1").GetComponent<EnergyController>();
        datStyle = new GUIStyle();
        datStyle.fontSize = 40;
        datStyle.font = datFont;
        datStyle.normal.textColor = Color.red;

        selectedButton = 0;
        activatedButton = -1;
        lastSelectionTime = 0f;
    }

    void Update()
    {
        if (!player.isAlive)
        {
            SetDeathButtonByInputs();
            SetDeathButtonByMouse();
            ActivateButton();
        }
    }

    // Diplay GUI for Snake prefab this script is atached to.
    void OnGUI()
    {
        if (!player.isAlive)
            DisplayDeathGui();
        DisplayScore();
        DisplayEnergy();
    }

    private void ActivateButton()
    {
        float select = Input.GetAxis("SelectGuiButton");
        if (select > 0.1f || select < -0.1f)
            activatedButton = selectedButton;
    }

    private void SetDeathButtonByMouse()
    {
        mousePosition = new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y);

        for (int i = 0; i < buttons.Length; i++)
        {
            if (buttons[i].rect.Contains(mousePosition))
            {
                selectedButton = i;
            }
        }
    }

    // Manage keyboard and joysticks for death Gui.
    private void SetDeathButtonByInputs()
    {
        float h = Input.GetAxis("HorizontalGui");
        float v = Input.GetAxis("VerticalGui");
        int nextButton = 0;
        float selectionTime;

        if (h > 0.1f || v > 0.1f)
            nextButton = 1;
        if (h < -0.1f || v < -0.1f)
            nextButton = -1;
        selectionTime = Time.time - lastSelectionTime;
        if (nextButton != 0 && selectionTime > 0.5f)
        {
            selectedButton = (selectedButton + nextButton + buttons.Length) % (buttons.Length);
            lastSelectionTime = Time.time;
        }
        if (nextButton == 0 && selectionTime < 0.5f)
            lastSelectionTime = 0f;
    }

    // Dislay button restart and quit when player dies.
    private void DisplayDeathGui()
    {
        GUI.Label(new Rect(25, 25, 100, 30), "YOu ArE DeAD");

        for (int i = 0 ; i < buttons.Length ; i++)
        {
            GUI.SetNextControlName(buttons[i].name);
            if (GUI.Button(buttons[i].rect, buttons[i].name) || activatedButton == i)
                buttons[i].func();
        }
        GUI.FocusControl(buttons[selectedButton].name);
    }

    // Display Snake score.
    private void DisplayScore()
    {
        GUI.Label(new Rect(Screen.width - 80, 25, 100, 30),
        "Score: " + score.score.ToString());
    }

    // restart game, associated to restart button.
    // OnRestart event have to bet filled with delegeates. See RestartAction.
    private void RestartButtonAction()
    {
        if (OnRestart != null)
            OnRestart();
    }

    // quit game associated to quit button.
    private void QuitButtonAction()
    {
        Application.Quit();
    }

    private void DisplayEnergy()
    {
        if (energy == null)
        {
            energy = GameObject.Find("Snake1").GetComponent<EnergyController>();
        }
        datStyle.normal.textColor = Color.Lerp(Color.blue, Color.red, (energy.volatilePower / 30f));
        GUI.Label(new Rect(50, 70, 100, 50), energy.volatilePower.ToString("0.0"), datStyle);
    }
}
