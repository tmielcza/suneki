﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Waits for a restart event in GuiLayer class and destroy the object it is attached on.
/// Add it to the gameobject you want in Unity.
/// </summary>
public class OnRestartDestroy : MonoBehaviour
{
    // Add DestroyEntity() to the list of delegates for OnRestart event in GuiLayer class.
    void OnEnable()
    {
        GuiLayer.OnRestart += DestroyEntity;
    }

    // Removes DestroyEntity() to the list of delegates for Onrestart event in GuiLayer class.
    void OnDisable()
    {
        GuiLayer.OnRestart -= DestroyEntity;
    }

    // Destroy current object.
    // This fonction need to be used as delegate for GuiLayer class OnRestart event.
    // Simply add OnRestartDestroy script to the gameobject needed in Unity.
    private void DestroyEntity()
    {
        Destroy(this.gameObject);
    }
}
